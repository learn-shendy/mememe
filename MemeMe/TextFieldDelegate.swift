//
//  TextFieldDelegate.swift
//  MemeMe
//
//  Created by  Ahmed Shendy on 9/8/18.
//  Copyright © 2018 Ahmed Shendy. All rights reserved.
//

import Foundation
import UIKit

class TextFieldDelegate: NSObject, UITextFieldDelegate {
    
    //MARK: Properties
    var isTextFieldEditedList = [false, false]  // index 0 for topTextField, index 1 for bottomTextField
    
    //MARK: Delegate Methods
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.updateFocusIfNeeded()
        if !isTextFieldEditedList[textField.tag] {
            textField.text = ""
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if (textField.text!.count > 0) {
            setTextFieldAsEdited(textField)
        } else {
            setDefaultTextOfTextField(textField)
        }
        
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    //MARK: Private Methods
    
    private func setTextFieldAsEdited(_ textField: UITextField) {
        isTextFieldEditedList[textField.tag] = true
    }
    
    private func setDefaultTextOfTextField(_ textField: UITextField) {
        textField.text = defaultText(of: textField)
    }
    
    private func defaultText(of textField: UITextField) -> String {
        return AddMemeViewController.textFieldDefaultTextList[textField.tag]
    }
}
