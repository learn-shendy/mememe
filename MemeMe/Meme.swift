//
//  Meme.swift
//  MemeMe
//
//  Created by  Ahmed Shendy on 9/11/18.
//  Copyright © 2018 Ahmed Shendy. All rights reserved.
//

import Foundation
import UIKit

struct Meme {
    let topText: String
    let bottomText: String
    let originalImage: UIImage
    let memedImage: UIImage
}
