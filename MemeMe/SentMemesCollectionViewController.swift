//
//  SentMemesCollectionViewController.swift
//  MemeMe
//
//  Created by  Ahmed Shendy on 9/29/18.
//  Copyright © 2018 Ahmed Shendy. All rights reserved.
//

import UIKit

let reuseIdentifier = "MemeCollectionCell"

class MemeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var memedImageView: UIImageView!
}

class SentMemesCollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    //MARK: Outlets
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    
    
    //MARK: Properties
    
    var sentMemes: [Meme]! {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.sentMemes
    }

    //MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let space: CGFloat = 3.0
        let dimension = (view.frame.size.width - (2 * space)) / 3.0

        flowLayout.minimumInteritemSpacing = space
        flowLayout.minimumLineSpacing = space
        flowLayout.itemSize = CGSize(width: dimension, height: dimension)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.collectionView.reloadData()
    }

    // MARK: Collection View Data Source

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sentMemes.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MemeCollectionViewCell
        let meme = sentMemes[indexPath.row]
        
        cell.memedImageView.image = meme.memedImage
        
        return cell
    }
}
